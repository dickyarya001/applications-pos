<?php

namespace App\Imports;

use App\Models\CrmPoin;
use App\Models\Product;
use App\Models\ProductType;
use App\Models\SalesProduct;
use App\Models\User;
use App\Models\UserHistory;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $checkType = ProductType::where('kode_produk', $row['kode_produk'])->count();
        if($checkType == 0)
        {
            $newType['kode_produk'] = $row['kode_produk'];
            $newType['nama_produk'] = $row['nama_produk'];
    
            ProductType::create($newType);

            $groups = User::groupBy('id_group')->get();
            // dd($groups);
            foreach($groups as $group)
            {
                $newProduct['id_productType'] = ProductType::where('kode_produk', $row['kode_produk'])->first()->id;
                $newProduct['id_group'] = $group->id_group;

                $owner = User::where('id_group', $group->id_group)->first();
                $newProduct['id_owner'] = $owner->id;

        // CREATE NEW PRODUCT PUSAT
                if($group->group->nama_group == "pusat")
                {
                    $newProduct['stok'] = $row['stok'];
                    $newProduct['harga_jual'] = $row['harga_jual'];
                    $newProduct['harga_modal'] = $row['harga_modal'];
                    if($row['stok'] == null || $row['stok'] == 0)
                    {
                        $newProduct['keterangan'] = 'Kosong';
                    }
                    else
                    {
                        $newProduct['keterangan'] = 'Tersedia';
                    }
                }
        // CREATE NEW PRODUCT DISTRIBUTOR, HARGA MODAL = HARGA JUAL PUSAT
                else if($owner->user_position == "superadmin_distributor")
                {
                    $newProduct['stok'] = 0;
                    $newProduct['harga_jual'] = 0;
                    $newProduct['harga_modal'] = $row['harga_jual'];
                    $newProduct['keterangan'] = 'Kosong';

                    $salesProduct['id_productType'] = $newProduct['id_productType'];
                    $salesProduct['id_group'] = $newProduct['id_group'];
                    $salesProduct['id_owner'] = $newProduct['id_owner'];
                    $salesProduct['harga_jual'] = 0;
                    $salesProduct['bonus'] = 0;

                    SalesProduct::create($salesProduct);
                }

                Product::create($newProduct);

                $resellers = User::where('id_group', $owner->id_group)->where('user_position', 'reseller')->get();
                // dd($resellers);

        // CREATE NEW PRODUCT RESELLER, HARGA MODAL = 0
                foreach($resellers as $reseller)
                {
                    $newProductReseller['id_productType'] = ProductType::where('kode_produk', $row['kode_produk'])->first()->id;
                    $newProductReseller['id_group'] = $owner->id_group;
                    $newProductReseller['id_owner'] = $reseller->id;
                    $newProductReseller['stok'] = 0;
                    $newProductReseller['harga_jual'] = 0;
                    $newProductReseller['harga_modal'] = 0;
                    $newProductReseller['keterangan'] = 'Kosong';

                    Product::create($newProductReseller);
                }

        // CREATE NEW PRODUCT SALES, HARGA MODAL = HARGA JUAL PUSAT
                $sales = User::where('id_group', $owner->id_group)->where('user_position', 'sales')->get();

                foreach($sales as $s)
                {
                    $newProductSales['id_productType'] = ProductType::where('kode_produk', $row['kode_produk'])->first()->id;
                    $newProductSales['id_group'] = $owner->id_group;
                    $newProductSales['id_owner'] = $s->id;
                    $newProductSales['stok'] = 0;
                    $newProductSales['harga_jual'] = 0;
                    $newProductSales['harga_modal'] = $row['harga_jual'];
                    $newProductSales['keterangan'] = 'Kosong';

                    Product::create($newProductSales);
                }
            }
            
        // CREATE POIN CRM = 0
            $newPoinCrm['id_productType'] = ProductType::where('kode_produk', $row['kode_produk'])->first()->id;
            $newPoinCrm['distributor_beli'] = 0;
            $newPoinCrm['distributor_jual'] = 0;
            $newPoinCrm['reseller_beli'] = 0;
            CrmPoin::create($newPoinCrm);

            $newActivity['id_user'] = auth()->user()->id;
            $newActivity['id_group'] = auth()->user()->id_group;
            $newActivity['kegiatan'] = "Create product '".$row['nama_produk']."'";
            UserHistory::create($newActivity);
    }
    }
}

