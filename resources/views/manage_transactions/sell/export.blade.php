<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Transaksi</title>
    <style>
        #myTable thead, #myTable tbody,  #myTable tr,  #myTable th, #myTable td{
            border: 1px solid black;
        }
    </style>
</head>

<body style="background-color:white">
    <table>
        <tr>
            <td>
                Riwayat Sell Transaksi
            </td>
        </tr>
    </table>
    <br>
    <table>
        <thead>
            <tr>
                <th>Kode Transaksi</th>
                <th>Total</th>
                <th>Diskon</th>
                <th>Keterangan</th>
                <th>Tanggal</th>
                <th>Metode Pembayaran</th>
            </tr>
        </thead>
        <tbody>
            @foreach($histories as $history)
            <tr id="{{ $history->id }}">
                <td>{{ $history->transaction_code }}</td>
                <td>Rp. {{ number_format($history->total, 0, ',', '.') }}</td>
                <td>Rp. {{ number_format($history->diskon, 0, ',', '.') }}</td>
                <td>{{ $history->keterangan_diskon }}</td>
                <td>{{ $history->updated_at->format('d/m/y H:i:s') }}</td>
                <td>{{ $history->metode_pembayaran }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>